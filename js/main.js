$(document).ready( function () {
	
	// owl-carousel banner
	$(".banner-owl-carousel").owlCarousel({
		items:1,
		loop:true,
		nav:true,
		dots:false,
		autoplay:true,
		autoplaySpeed:1000,
		smartSpeed:1500,
		autoplayHoverPause:true,
		animateOut: 'fadeOut',
		animateIn: 'fadeIn',
		autoHeight: false,
	});

	// top-sale owl-carousel
	$(".tap-sale-owl-carousel").owlCarousel({
		loop:true,
		dots:false,
		autoplay:true,
		responsive: {
			0: {
				items: 1
			},
			600: {
				items: 3
			},
			1000:{
				items: 4
			}
		}
	});

	// for fileter product
	let $grid = $('.grid').isotope({
		itemSelector: '.item',
		//layoutMode : 'fitRows'
	});
	$('.button-group').on('click', 'button', function() {
		let getFilterValue = $(this).attr('data-filter');
		$grid.isotope({filter : getFilterValue});
		$('.button-group button').removeClass('active-btn');
		$(this).addClass('active-btn');
	
	});
	
	// new product owl-carousel
	$(".new-product-owl-carousel").owlCarousel({
		loop:true,
		dots:false,
		nav:true,
		autoplay:true,
		responsive: {
			0: {
				items: 1
			},
			600: {
				items: 3
			},
			1000:{
				items: 4
			}
		}
	});

	// product gallery owl-carousel
	$(".product-gallery-slider").owlCarousel({
		items: 3,
		loop:true,
		nav:true,
		dots:false,
		navText:['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
		autoplay:true
	});

	$('.product-gallery-slider .pt').on('click', function () {
		$('.product-gallery-slider .pt').removeClass('active-product-pic');
		$(this).addClass('active-product-pic');
		let getImageName = $(this).attr('src');
		let product_pic = $('.product-pic img').attr('src');
		
		if ( getImageName != product_pic ) {
			$('.product-pic img').attr({src:getImageName});
		}
	});

	// product quentity
	let proQty, btn, oldVal, newVal;
	proQty = $('.product-qty');
	proQty.on('click', '.qty', function () {
		btn = $(this);
		oldVal = btn.parent().find('input').val();
		if ( btn.hasClass('qty-up') ) {
			newVal = parseInt( oldVal ) + 1 ;
		} else {
			if ( oldVal > 1 ) {
				newVal = parseInt( oldVal ) - 1 ;
			} else {
				newVal = 1;
			}
		}
		btn.parent().find('input').val( newVal );
	});


});